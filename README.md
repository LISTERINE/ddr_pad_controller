# ddr pad controller
Code for a Teensy3.2/Teensy-LC to run a DDR arcade stage

What this does:
---------------
Works with Teensy3.2 and Teensy-LC.

The teensy will emulate a USB keyboard allowing stage arrows to send a unique keycode to a computer.

When a stage arrow is pressed the teensy will also trigger a transistor to light up the selected stage arrow (if using the companion hardware described in this repository).

What this does NOT do:
----------------------
Comunicate with a system 573 to run the dance stages using the cabinets hardware.


## Overview:
### Configuration:
By default the software will send the following keys for the given arrows on the given pad(Pn):

|       | P1        | P2    |
|:-----:|-----------|-------|
| Up    | Key up    | Key w |
| Down  | Key down  | Key s |
| Left  | Key left  | Key a |
| Right | Key right | Key d |

##### Part list (per board) (if fabricating companion hardware):
* 4x tip 120 transistors
* 1x teensy3.2/LC
* 4x 1kohm resistors
* 1x B4P-VH(LF)(SN)
* 4x B14P-SHF-1AA

### Setup:
##### option 1 (without fabricating boards):
* Flash two Teensys with the P1 and P2 code respectivly (found in `./controller_programs/`).
* Wire ground from the teensy to each pad connector, wire black(0)
* Wire pins 0,18,19,20 to right down, connector wires green(1), blue(2), purple(3), grey(4).
* Wire pins 1-4 to pad left, connector wires green(1), blue(2), purple(3), grey(4).
* Wire pins 5-8 to pad down, connector wires green(1), blue(2), purple(3), grey(4).
* Wire pins 9-12 to pad up, connector wires green(1), blue(2), purple(3), grey(4).
* Connect the Teensy to the computer.

##### option 2 (if fabricating boards):
* Select one of the three board designs (`./board_design_files`)
  * Mini (tested) <a href="https://oshpark.com/shared_projects/UG4QKk7c"><img src="https://oshpark.com/assets/badge-5b7ec47045b78aef6eb9d83b3bac6b1920de805e9a0c227658eac6e19a045b9c.png" alt="Order from OSH Park"></img></a>
  * Standard (untested) <a href="https://oshpark.com/shared_projects/i2YYBC9R"><img src="https://oshpark.com/assets/badge-5b7ec47045b78aef6eb9d83b3bac6b1920de805e9a0c227658eac6e19a045b9c.png" alt="Order from OSH Park"></img></a>
  * Full size (untested) <a href="https://oshpark.com/shared_projects/uzx6CiTk"><img src="https://oshpark.com/assets/badge-5b7ec47045b78aef6eb9d83b3bac6b1920de805e9a0c227658eac6e19a045b9c.png" alt="Order from OSH Park"></img></a>
* Purchase all parts in the parts list shown at above in the [configuration section](#part-list-per-board-if-fabricating-companion-hardware)
* Build instructions at http://imgur.com/a/Jxcba

##### How-to for flashing code to a Teensy can be found here:

https://www.pjrc.com/teensy/first_use.html

https://www.pjrc.com/teensy/loader.html





