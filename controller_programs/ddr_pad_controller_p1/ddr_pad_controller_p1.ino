/* Buttons to USB Keyboard Example

   You must select Keyboard from the "Tools > USB Type" menu

   This example code is in the public domain.
*/



byte up = KEY_UP;
byte down = KEY_DOWN;
byte left = KEY_LEFT;
byte right = KEY_RIGHT;
  

void setup() {
  pinMode(0, INPUT_PULLUP);
  pinMode(1, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  pinMode(11, INPUT_PULLUP);
  pinMode(12, INPUT_PULLUP); // skip over 12/13. Teensy++ LED, may need 1k resistor pullup. just move to 14 to avoid extra soldering. skip 12 to keep wires grouped.
  pinMode(18, INPUT_PULLUP);
  pinMode(19, INPUT_PULLUP);
  pinMode(20, INPUT_PULLUP);
  // Lights
  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);


}

void loop() {
  // Update all the buttons.  There should not be any long
  // delays in loop(), so this runs repetitively at a rate
  // faster than the buttons could be pressed and released.
  /*
  button0.update();
  button1.update();
  button2.update();
  button3.update();
  button4.update();
  button5.update();
  button6.update();
  button7.update();
  button8.update();
  button9.update();
  button10.update();
  button11.update();
  button12.update();
  button13.update();
  button14.update();
  button15.update();
  */
  /******************************************************
   *
   * Determine which pads/sensors were pressed/released
   *
   ******************************************************/

   
  // Check pad left
  if ((digitalRead(1) == LOW) || (digitalRead(2) == LOW) || (digitalRead(3) == LOW) || (digitalRead(4) == LOW)) {
    Keyboard.set_key1(left);
    digitalWrite(16, HIGH);   // set the LED ON
  }
  else {
    Keyboard.set_key1(0); // Release
    digitalWrite(16, LOW);   // set the LED OFF
  }

  // Check pad right
  if ((digitalRead(0) == LOW) || (digitalRead(18) == LOW) || (digitalRead(19) == LOW) || (digitalRead(20) == LOW)) {
    Keyboard.set_key2(right);
    digitalWrite(17, HIGH);   // set the LED ON
  }
  else {
    Keyboard.set_key2(0); // Release
    digitalWrite(17, LOW);   // set the LED OFF
  }

  // Check pad down
  if ((digitalRead(5) == LOW) || (digitalRead(6) == LOW) || (digitalRead(7) == LOW) || (digitalRead(8) == LOW)) {
    Keyboard.set_key3(down);
    digitalWrite(15, HIGH);   // set the LED ON
  }
  else {
    Keyboard.set_key3(0); // Release
    digitalWrite(15, LOW);   // set the LED OFF
  }

  // Check pad up
  if ((digitalRead(9) == LOW) || (digitalRead(10) == LOW) || (digitalRead(11) == LOW) || (digitalRead(12) == LOW)) {
    Keyboard.set_key4(up);
    digitalWrite(14, HIGH);   // set the LED ON
  }
  else {
    Keyboard.set_key4(0); // Release
    digitalWrite(14, LOW);   // set the LED OFF
  }
  
  Keyboard.send_now(); // Send the keys that have been set
}
